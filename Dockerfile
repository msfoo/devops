# docker build -t health-check -f Dockerfile .
# docker run -p 6734:5000 health-check

FROM rust:1.75-alpine3.19 AS build

EXPOSE 6734
RUN apk add --no-cache \
        ca-certificates \
        gcc

RUN apk add musl-dev

WORKDIR /usr/src/myapp
COPY . .

RUN cargo build --release

CMD ["cargo", "run"]
# FROM alpine:3.19.1 AS final

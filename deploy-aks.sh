#!/bin/bash
set -e

TEAM_NAME=$1
ACR_NAME=$2
DOMAIN_NAME=$3

IMAGE_TAG="latest"
PROJECT_NAME="devops"
CLUSTER_NAME="team"

REPO_NAME="${ACR_NAME}"
IMAGE_NAME="health-check"

# TODO Log into the ACR
az acr login -n "$ACR_NAME"

# TODO Build the image
docker build --target final -t "$IMAGE_NAME" .
# TODO Push the image to the ACR
docker push "$IMAGE_NAME"

# TODO Retrieve kubernetes credentials / add them to .kubeconfig
az aks get-credentials -g "CS-01-rg" -n "teamcluster"

# TODO Deploy or upgrade helm charts
if ! (helm ls  | grep $PROJECT_NAME) then
   eval helm install -f helm/values.yaml $PROJECT_NAME helm/ $VARIABLES
else
   eval helm upgrade -f helm/values.yaml $PROJECT_NAME helm/ $VARIABLES
fi
